cdist-type__install_binaries_from_release(7)
============================================

NAME
----
cdist-type__install_binaries_from_release - Install useful binaries from GitHub.


DESCRIPTION
-----------
Install useful binaries from GitHub releases.

All binaries are opt-in.


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------

destdir
   the remote destination path where the binaries will be installed,
   default to ``/usr/local/bin``.

For each of the following, the version is required as an argument.

bat_
   a ``cat(1)`` clone with wings

digikam_
   an image manager for managing collections

fd_
   simple, fast and user-friendly alternative to ``find(1)``

jq_
   a lightweight and flexible command-line JSON processor

rg_
   a line-oriented search tool that recursively searches the current
   directory for a regex pattern

shellcheck_
   a static analysis tool for shell scripts

watchexec_
   executes commands in response to file modifications

.. _bat: https://github.com/sharkdp/bat/releases
.. _fd: https://github.com/sharkdp/fd/releases
.. _jq: https://github.com/stedolan/jq/releases
.. _rg: https://github.com/BurntSushi/ripgrep/releases
.. _shellcheck: https://github.com/koalaman/shellcheck/releases
.. _watchexec: https://github.com/watchexec/watchexec/releases

BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

   # install watchexec only for root
    __install_binaries_from_release \
      --destdir /root/.local/bin \
      --watchexec 1.21.0

   # install everything
    __install_github_release_bin \
      --bat 0.22.1 \
      --fd 8.6.0 \
      --jq 1.6 \
      --rg 13.0.0 \
      --shellcheck 0.9.0 \
      --watchexec 1.17.1

   # quick install bat on localhost
   echo '__install_binaries_from_release --bat 0.22.1' |
   cdist config -i - localhost

AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2021 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
