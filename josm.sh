#!/bin/sh

# https://josm.openstreetmap.de/wiki/Download#Ubuntu
wget -q https://josm.openstreetmap.de/josm-apt.key -O- | apt-key add -

codename=
[ -e /etc/upstream-release/lsb-release ] &&
codename=$(sed -n 's/^DISTRIB_CODENAME=//p' /etc/upstream-release/lsb-release)

[ -z "$codename" ] &&
codename=$(lsb_release -sc)

echo "deb https://josm.openstreetmap.de/apt ${codename} universe" |
tee /etc/apt/sources.list.d/josm.list > /dev/null

apt-get update
apt-get install josm
